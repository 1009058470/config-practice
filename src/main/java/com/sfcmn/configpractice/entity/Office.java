package com.sfcmn.configpractice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author bo.wang
 */
@Entity
public class Office {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @Size(min = 1, max = 32)
    @Column(nullable = false, length = 32)
    private String country;
    @NotNull
    @Size(min = 1, max = 32)
    @Column(nullable = false, length = 32)
    private String city;

    public Office() {
    }

    public Office(@NotNull @Size(min = 1, max = 32) String country, @NotNull @Size(min = 1, max = 32) String city) {
        this.country = country;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }
}
