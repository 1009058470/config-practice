package com.sfcmn.configpractice.web;

import com.sfcmn.configpractice.entity.Office;
import com.sfcmn.configpractice.entity.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ConfigDBController {
    @Autowired
    private OfficeRepository repo;

    @PostMapping("/offices")
    public ResponseEntity createOffice(@RequestBody @Valid Office office) {
        @Valid Office savedOffice = repo.save(office);
        repo.flush();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("location", "http://localhost/api/offices/" + savedOffice.getId())
                .build();
    }

}
