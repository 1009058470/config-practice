package com.sfcmn.configpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigPracticeApplication.class, args);
	}

}
